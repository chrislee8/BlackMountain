/*
*   FullScreen for Home section
*/
function homeFullScreenHeight() {
    $('#home').css({ height: $(window).height() });
}

homeFullScreenHeight();

$(window).bind('resize', function() {
    homeFullScreenHeight();
});

/*
*   Sticky Menu section
*/
$("nav.sticky-nav").sticky({
    topSpacing: 0,
    className: 'sticky',
    wrapperClassName: 'menu-wrapper'
})